#!/bin/bash

while read box
do
  if [ "$2" = "show" ]; then
     roger show $box
  else
    case "$4" in
            "intervention")
              roger $2 $box --all_alarms $3 --appstate $4 --duration $5 --message "$6"
            ;;
            "production")
              roger $2 $box --all_alarms $3 --appstate $4
            ;;
            *)
              echo "appstate $4 non expected"
              exit 1
    esac
  fi

done < $1

