# What?

Bunch of (hopefully) useful scripts for helping to manage the interventions related with the [https://cern.service-now.com/service-portal/service-element.do?name=Databases-Applications](Databases Applications Service)

*IMPORTANT:* the starting point for the procedure documentation must be: https://twiki.cern.ch/twiki/bin/view/DB/Private/IMSPolicyProcedure

# Why?

Automate all the things! Or at least help myself to be a bit more productive

# How?

## Quick reboot interventions

See https://twiki.cern.ch/twiki/bin/view/DB/Private/QuickRebootIntervention


## Appdynamics installations

https://twiki.cern.ch/twiki/bin/view/DB/Private/AppDynamics

# Who?

luis.rodriguez.fernandez@cern.ch
