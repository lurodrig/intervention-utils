#!/bin/sh

# Get the entity and its type: RDBMS, APPSERVER, CMAN...
sc_entities=`vegas-cli tab sc_host_name=$1 sc_entity`
for sc_entity in ${sc_entities[@]}
do 
  sc_subcategory=`vegas-cli tab sc_entity=$sc_entity sc_subcategory`
  echo "$sc_entity is $sc_subcategory"
  case "$sc_subcategory" in
                       "WLS_DOMAIN")
                       machines=`vegas-cli wlstab sc_entity=$sc_entity sc_subcategory=$sc_subcategory machine_name`
                       for machine in ${machines[@]}
                       do
                         ihost $machine
                       done
                       ;; 
                       *)
                       echo "sc_subcategory=$sc_subcategory"
  esac
done


